#!/bin/bash

# TODO Check for existence of config.yml and cluster.yml

# TODO Optional parameters to specify config and cluster files

# Workaround for conda as a shell function
#if ! command -v conda > /dev/null 2>&1; then
#    echo "Add conda to path"
#    export PATH="${HOME}/miniconda3/bin;${PATH}"
#fi

snakemake --cluster-config 'cluster_config.cetus.yml' \
          --drmaa " --cpus-per-task={cluster.n} --mem={cluster.memory} --qos={cluster.qos} --time={cluster.time}" \
          --use-conda -w 60 -rp -j 500 "$@"

