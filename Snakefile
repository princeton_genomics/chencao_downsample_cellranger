# The main entry point of your workflow for Downsample 10x CellRanger analyses
# After configuring, running snakemake -n in a clone of this repository should
# successfully execute a dry-run of the workflow.

configfile: "config.yml"

import re
import pandas as pd
samples = pd.read_table("samples.tsv").set_index("samplename", drop=False)

localrules: all, mkfastq, cellrangner_count, cellranger_aggregate, cellranger_aggregate_input_csv

rule all:
    input:
        # fastq1=expand("{sample.samplename}/fastq/full/{sample.samplename}_S1_L001_R1_001.fastq.gz", sample=samples.itertuples()),
        # fastq2=expand("{sample.samplename}/fastq/full/{sample.samplename}_S1_L001_R2_001.fastq.gz", sample=samples.itertuples())
        # expand("{sample.samplename}/fastq/downsample_{downsample_amount}/{sample.samplename}_downsample_{downsample_amount}_S1_L001_{read}_001.fastq.gz", sample=samples.itertuples(), downsample_amount=config["downsample_amounts"], read=["R1","R2"])
        expand("{sample.samplename}/{sample.samplename}_downsample_{downsample_amount}/outs/molecule_info.h5",
               sample=samples.itertuples(),
               downsample_amount=config["downsample_amounts"]),
        expand("aggr/aggr_LTB2_with_LTB2_2_downsample_{downsample_amount}",
               downsample_amount=config["downsample_amounts"])

include: "rules/mkfastq.smk"
include: "rules/downsample.smk"
include: "rules/cellranger.smk"

