def get_fastq(wildcards):
    return samples.loc[(wildcards.samplename), ["R1", "R2"]].dropna()

rule mkfastq:
    input: get_fastq
    output:
        fastq1="{samplename}/fastq/full/{samplename}_S1_L001_R1_001.fastq.gz",
        fastq2="{samplename}/fastq/full/{samplename}_S1_L001_R2_001.fastq.gz"
    shell:
        "ln -s -f ../../../{input[0]:q} {output.fastq1:q} && "
        "ln -s -f ../../../{input[1]:q} {output.fastq2:q}"

