rule seqtk_downsample_fastq:
    input: "{samplename}/fastq/full/{samplename}_S1_L00{lane}_{read}_001.fastq.gz"
    output: temp("{samplename}/fastq/downsample_{downsample_amount}/{samplename}_downsample_{downsample_amount}_S1_L00{lane}_{read}_001.fastq.gz")
    threads: 4
    shell:
        "seqtk sample -s{config[random_seed]} {input:q} 0.{wildcards.downsample_amount} | pigz -p {threads} > {output:q}"

