import re

rule cellranger_count:
    input:
        R1="{samplename}/fastq/downsample_{downsample_amount}/{samplename}_downsample_{downsample_amount}_S1_L001_R1_001.fastq.gz",
        R2="{samplename}/fastq/downsample_{downsample_amount}/{samplename}_downsample_{downsample_amount}_S1_L001_R2_001.fastq.gz"
    output:
        molecule_info="{samplename}/{samplename}_downsample_{downsample_amount}/outs/molecule_info.h5"
    params:
        fastq_dir="fastq/downsample_{downsample_amount}"
    shell:
        "module load cellranger/2.1.1 && "
        "cd {wildcards.samplename} && "
        "cellranger count "
            "--id={wildcards.samplename}_downsample_{wildcards.downsample_amount} "
            "--sample={wildcards.samplename}_downsample_{wildcards.downsample_amount} "
            "--fastqs={params.fastq_dir} "
            "--transcriptome={config[transcriptome]} "
            "--jobmode=slurm "
            "--disable-ui "


rule cellranger_aggregate_input_csv:
    input:
        sample1="{samplename1,[^/]}/{samplename1}_downsample_{downsample_amount}/outs/molecule_info.h5",
        sample2="{samplename2,[^/]}/{samplename2}_downsample_{downsample_amount}/outs/molecule_info.h5"
    output:
        temp("aggr/file_list-aggr_{samplename1}_with_{samplename2}_downsample_{downsample_amount}.csv")
    run:
        with open(output[0], "w") as out:
            out.write("library_id,molecule_h5\n")
            out.write("{},../{}\n".format(wildcards.samplename1, input.sample1))
            out.write("{},../{}\n".format(wildcards.samplename2, input.sample2))


rule cellranger_aggregate:
    input:
        "aggr/file_list-aggr_{samplename1}_with_{samplename2}_downsample_{downsample_amount}.csv"
    output:
        directory("aggr/aggr_{samplename1}_with_{samplename2}_downsample_{downsample_amount}")
    params:
        aggr_id="aggr_{samplename1}_with_{samplename2}_downsample_{downsample_amount}"
    shell:
        "module load cellranger/2.1.1 && "
        "cd aggr && "
        "cellranger aggr "
            "--id={params.aggr_id:q} "
            "--csv=../{input:q} "
            "--jobmode=slurm "
            "--disable-ui"

